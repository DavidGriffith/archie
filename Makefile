# New modernized Makefile for Archie
# GNU make is required.

# Choose your preferred C compiler
#CC = gcc
#CC = clang

PREFIX ?= /usr/local
MANDIR ?= $(MAN_PREFIX)/man
BINDIR ?= $(PREFIX)/bin
LIBDIR ?= $(PREFIX)/lib
MAN_PREFIX ?= $(PREFIX)/share
SYSCONFDIR ?= /etc


NAME = archie
VERSION = 3.5

SUBDIRS = lib/archsearch

#	anonftp lib/archsearch clients control doc exchange lib/hostdb \
#	include less lib/libarchie lib/libparchie \
#	lib/libpsarchie lib/patrie ppc lib/regex lib/reposix lib/startdb \
#	lib/archstridx tools webindex

DOC_MODULE = doc

DISTRIBUTION_MODULE = dist

BIN_SING_LEVEL_MODULES = less control exchange tools

BIN_MULTI_LEVEL_MODULES = clients ppc

LIB_SING_LEVEL_MODULES = lib/archsearch lib/hostdb lib/libarchie \
	lib/libparchie lib/libpsarchie lib/patrie lib/regex lib/reposix \
	lib/startdb lib/archstridx

LIB_MULTI_LEVEL_MODULES = anonftp webindex


LIB_ARCHSEARCH_DIR = lib/archsearch
LIB_ARCHSEARCH_LIB = $(LIB_ARCHSEARCH_DIR)/libarchsearch.a

LIB_HOSTDB_DIR = lib/hostdb
LIB_HOSTDB_LIB = $(LIB_HOSTDB_DIR)/hostdb.a

# The modules are grouped into three classes, single level, multi-level and
# generic.  Single level modules are directories, containing source code that
# compiles to object files, immediately below the top level.  Multi-level
# modules are similar, but they are subdirectories of directories at the top
# level.  Generic modules are directories containing files that don't produce
# object files.
#
SING_LEVEL_MODULES = $(LIB_SING_LEVEL_MODULES) $(BIN_SING_LEVEL_MODULES)

MULTI_LEVEL_MODULES = $(LIB_MULTI_LEVEL_MODULES) $(BIN_MULTI_LEVEL_MODULES)


# GEN_MODULES is the list of modules that are system independent.
#
GEN_MODULES = doc include

# Auxilliary programs.
#
AR       = ar
LD       = ld
RANLIB   = ranlib
SENTINEL = memadvise


#########################
## New stuff begin here
#########################


ROOT_DIR := ${CURDIR}
SUB_CLEAN = $(SUBDIRS:%=%-clean)

INCLUDES = -I $(ROOT_DIR)/include -I $(ROOT_DIR)/webindex/lib \
	-I $(ROOT_DIR)/lib/startdb -I $(ROOT_DIR)/lib/patrie \
	-I $(ROOT_DIR)/lib/archstridx

export CC
export CFLAGS
export MAKEFLAGS
export INCLUDES
export AR
export RANLIB
export PREFIX

help:
	@echo ""
	@echo "Possible targets are:  all  all_clean  all_depend  all_newsys all_dist"
	@echo ""

all: $(LIB_ARCHSEARCH_LIB) $(LIB_HOSTDB_LIB)
	@echo "Whee!"


$(LIB_ARCHSEARCH_LIB):
	$(MAKE) -C $(LIB_ARCHSEARCH_DIR)

$(LIB_HOSTDB_LIB):
	$(MAKE) -C$(LIB_HOSTDB_DIR)

clean: $(SUB_CLEAN)
$(SUB_CLEAN):
	-$(MAKE) -C $(@:%-clean=%) clean
